#include "Platform.h"
#include "AssetManager.h"

Platform::Platform()
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Platform.png"))
{

}

Platform::Platform(sf::Texture& newTexture)
	: SpriteObject(newTexture)
{

}

void Platform::Update(sf::Time frameTime)
{

}

void Platform::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}
