#pragma once
#include "SpriteObject.h"
#include <map>

class AnimatingObject :
    public SpriteObject
{
public:
    AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS);

    void Update(sf::Time frameTime);

    void AddClip(std::string name, int startFrame, int endFrame);

    void PlayClip(std::string name, bool shouldLoop = true);

    void Pause();
    void Stop();
    void Resume();


private:

    void UpdateSpriteTextureRect();

    struct Clip
    {
    public:
        int startFrame;
        int endFrame;
    };

    int frameWidth;
    int frameHeight;
    float framesPerSecond;
    int currentFrame;
    sf::Time timeInFrame;
    std::map<std::string, Clip> clips;
    std::string currentClip;
    bool playing;
    bool looping;
};

