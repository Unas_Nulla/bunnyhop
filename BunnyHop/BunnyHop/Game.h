#pragma once
#include <SFML/Graphics.hpp>
#include "LevelScreen.h"

class Game
{
	public:
	Game();

	sf::RenderWindow& GetWindow();

	void RunGameLoop();

	void Input();
	void Update();
	void Draw();

	private:
	sf::RenderWindow window;	
	sf::Clock gameClock;
	LevelScreen levelScreenInstance;
};

