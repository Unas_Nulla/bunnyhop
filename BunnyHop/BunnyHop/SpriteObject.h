#pragma once
#include <SFML/Graphics.hpp>
class SpriteObject
{
public:
	// Constructors / Destructors
	SpriteObject(sf::Texture& newTexture);
	// Functions
	void DrawTo(sf::RenderTarget& target);

	sf::FloatRect GetHitBox();

protected:
	// Data
	sf::Sprite sprite;

};