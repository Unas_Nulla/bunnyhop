#pragma once
#include "AnimatingObject.h"
class Player :
	public AnimatingObject
{
public:
	// Constructors / Destructors
	Player(sf::Vector2u screenSize);
	
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);

private:
	// Data
	sf::Vector2f velocity;
	float speed;
	float gravity;

	sf::Vector2f previousPosition;
};


