#pragma once
#include "SpriteObject.h"
class Platform :
    public SpriteObject
{
public:
    // Constructors / Destructors
    Platform();
    Platform(sf::Texture& newTexture);
    virtual void Update(sf::Time frameTime);
    void SetPosition(sf::Vector2f newPosition);

private:

};

