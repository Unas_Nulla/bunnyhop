#include "Game.h"

Game::Game()
	: window(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close)
	, gameClock()
	, levelScreenInstance(this)
{
	//Window setup
	window.setMouseCursorVisible(false);
}

void Game::RunGameLoop()
{
	// Repeat as long as the window is open
	while (window.isOpen())
	{
		Input();
		Update();
		Draw();
	}
}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}

		// Close game if escape key is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}
	}
	levelScreenInstance.Input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();
	levelScreenInstance.Update(frameTime);
}

void Game::Draw()
{
	window.clear();
	levelScreenInstance.DrawTo(window);
	window.display();
}

sf::RenderWindow& Game::GetWindow()
{
	return window;
}