#include "LevelScreen.h"
#include "Game.h"
#include "MovingPlatform.h"
#include <stdlib.h>

LevelScreen::LevelScreen(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, platforms()
	, camera(newGamePointer->GetWindow().getDefaultView())
	, platformGap(50)
	, platformGapIncrease(3)
	, highestPlatform(0)
	, platformBuffer(100)
{
	// Create the starting platform
	Platform* platformInstance = new Platform();

	// Calculate center of the screen
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	// Calculate position of platform to be centered
	newPosition.x -= platformInstance->GetHitBox().width / 2;
	newPosition.y -= platformInstance->GetHitBox().height / 2;

	// Add to the y position to lower the platform a bit
	const float PLATFORM_OFFSET = 250; // this can be adjusted as needed
	newPosition.y += PLATFORM_OFFSET;	
	
	// Set the new position of the platform
	platformInstance->SetPosition(newPosition);

	// Copy the starting platform into the platform collection
	platforms.push_back(platformInstance);

	// Populate the rest of the platforms
	highestPlatform = newPosition.y;

	// Place platforms until the highest one is outside the camera buffer zone
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;

	while (highestPlatform > cameraTop - platformBuffer)
	{
		AddPlatform();
	}
}


void LevelScreen::Input()
{
	playerInstance.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);
	for (int i = 0; i < platforms.size(); i++)
	{
		playerInstance.HandleSolidCollision(platforms[i]->GetHitBox());
	}

	// Place a new platform if needed
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;

	if (highestPlatform > cameraTop - platformBuffer)
	{
		AddPlatform();
	}

	// If the bottom platform is off screen, remove it
	if (platforms[0]->GetHitBox().top > camera.getCenter().y + camera.getSize().y / 2)
	{
		// Delete the actual platform (freeing memory)
		delete platforms[0];

		// Erase the first element of the vector
		platforms.erase(platforms.begin());
	}

	// Update the platforms
	for (int i = 0; i < platforms.size(); ++i)
	{
		platforms[i]->Update(frameTime);
	}
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	// Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	float playerCenterY = playerInstance.GetHitBox().top + playerInstance.GetHitBox().height / 2;
	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}

	// Set camera view
	target.setView(camera);

	// Draw content to screen
	playerInstance.DrawTo(target);
	for (int i = 0; i < platforms.size(); i++)
	{
		platforms[i]->DrawTo(target);
	}

	// Remove camera view
	target.setView(target.getDefaultView());
}

void LevelScreen::AddPlatform()
{
	// Play area calculations
	int PLAY_AREA_WIDTH = 800;
	int minX = camera.getCenter().x - PLAY_AREA_WIDTH / 2;
	int maxX = camera.getCenter().x + PLAY_AREA_WIDTH / 2;

	// Create new platform
	Platform* newPlatform = nullptr;

	// Choose the type of platform to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceMoving = 50;
	
	if (choice < 50)
	{
		newPlatform = new MovingPlatform(minX, maxX);
	}
	else
	{
		newPlatform = new Platform();
	}

	sf::Vector2f newPosition;
	newPosition.y = highestPlatform - platformGap;

	// Randomise platform x position
	maxX -= newPlatform->GetHitBox().width;
	newPosition.x = rand() % (maxX - minX) + minX;

	// Set new platform position
	newPlatform->SetPosition(newPosition);
	
	// Add the new platform to the list
	platforms.push_back(newPlatform);

	// Update platform gap
	platformGap += platformGapIncrease;

	// Update highest platform
	highestPlatform = newPosition.y;
}