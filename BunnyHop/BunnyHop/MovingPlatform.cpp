#include "MovingPlatform.h"
#include "AssetManager.h"

MovingPlatform::MovingPlatform(float newMinX, float newMaxX)
	: Platform(AssetManager::RequestTexture("Assets/Graphics/MovingPlatform.png"))
	, speed(100)
	, minX(newMinX)
	, maxX(newMaxX)
{

}

void MovingPlatform::Update(sf::Time frameTime)
{
	//Record original position
	sf::Vector2f originalPosition = sprite.getPosition();

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition();
	newPosition.x += speed * frameTime.asSeconds();
	
	// Move the platform to the new position
	sprite.setPosition(newPosition);

	// Check is we are off screen to the right or left
	int platformLeft = GetHitBox().left;
	int platformRight = GetHitBox().left + GetHitBox().width;

	if (platformLeft < minX || platformRight > maxX)
	{
		// Turn armound (reverse speed)
		speed *= -1.0f;

		// Return to the position we were at before we went out of bounds
		sprite.setPosition(originalPosition);
	}
}