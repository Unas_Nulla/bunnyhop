#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"

class Game;

class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

private:
	Game* gamePointer;
	Player playerInstance;
	std::vector<Platform*> platforms;
	sf::View camera;

	float platformGap;
	float platformGapIncrease;
	float highestPlatform;
	float platformBuffer;

	void AddPlatform();
};

